import React from 'react';
import calculate from "../logic/calculate";


describe('Calculate Tests', ()=>{
    test('Number button tests', () => {
        expect(calculate({
                previous: null,
                current: null,
                operation: null},"4")).toHaveProperty('current', "4");
        expect(calculate({
                previous: null,
                current: 1,
                operation: null},"1")).toHaveProperty('current', "11");
        expect(calculate({
                previous: null,
                current: null,
                operation: "+"},"1")).toHaveProperty('current', "1");
    });

    test('Addition button tests', () => {
        expect(calculate({
                previous: null,
                current: 1,
                operation: null},"+")).toHaveProperty('current', null);

    });

    test('Equal button tests', () => {
        expect(calculate({
                previous: 1,
                current: 2,
                operation: "+"},"=")).toHaveProperty('previous', "3");
        expect(calculate({
                previous: 2,
                current: 2,
                operation: "+"},"=")).toHaveProperty('previous', "4");
    });

    test('Decimal point button test',() =>{
        expect(calculate({
                previous:"2",
                current:"1",
                operation: null},".")).toHaveProperty('current',"1.");
    });
    test('Decimal point button test',() =>{
        expect(calculate({
                previous:"1",
                current:"2",
                operation: null},".")).toHaveProperty('current',"2.");
    });
    test('Decimal point button test',() =>{ 
        expect(calculate({
                previous:"1",
                current:"3",
                operation: null},".")).toHaveProperty('current',"3.");
    });
    test('Decimal point button test',() =>{ 
        expect(calculate({
                previous:"1",
                current:"4",
                operation: null},".")).toHaveProperty('current',"4.");
    });
});


test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: null,
                current: 1,
                operation: null},"+/-")).toHaveProperty('current', "-1");

    });

    test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: 7,
                current: null,
                operation: null},"+/-")).toHaveProperty('previous', "-7");

    });

    test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: 8,
                current: 3,
                operation: null},"+/-")).toHaveProperty('current', "-3");

    });
    test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: null,
                current: -3,
                operation: null},"+/-")).toHaveProperty('current', "3");

    });