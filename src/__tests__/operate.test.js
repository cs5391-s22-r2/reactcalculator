import React from 'react';
import operate from "../logic/operate";
import Big from "big.js";



describe('Operate Multiplication Tests', ()=>{
    test('4 x 3 = 12 test', () => {
        expect(operate("4","3","x")).toBe("12"); 
    });
    test('16.2 x 6 = 97.2 test', () => {
        expect(operate("16.2","6","x")).toBe("97.2"); 
    });
   test('27.2 x 7 = 190.4 test', () => {
        expect(operate("27.2","7","x")).toBe("190.4"); 
    });
  test('36.4 x 8 = 291.2 test', () => {
        expect(operate("36.4","8","x")).toBe("291.2"); 
    });
    
});


describe('Operate Division Tests', ()=>{
    test('4/3 = 1.333 test', () => {
        expect(parseFloat(operate("4","3","÷"))).
            toBeCloseTo(1.3333333333, 5); 
        // parseFloat() is a string-to-float function in Javascript.   
        // Try different precisions and string lengths to see what the test does.
    });
    test('20/5 = 4 test', () => {
        expect(operate("20","5","÷")).toBe("4"); 
    });
   test('37/3 = 12.3333 test', () => {
        expect(parseFloat(operate("37","3","÷"))).toBeCloseTo(12.3333333,3); 
    });
  test('79/7 = 11.2857 test', () => {
        expect(parseFloat(operate("79","7","÷"))).toBeCloseTo(11.28576,3); 
    });
    
});

describe('Operate Addition Tests', ()=>{
    test('-4 + 3 = -1 test', () => {
        expect(operate("-4","3","+")).toBe("-1"); 
    });
    test('-8 + 4 = -4 test', () => {
        expect(operate("-8","4","+")).toBe("-4"); 
    });
});
describe('Operate addition Tests', ()=>{
    test('6+9 = 15 test', () => {
        expect(operate("6","9","+")).
            toBe("15"); 
    
    });
});

describe('Operate Substraction Tests', ()=>{
    test('17-3 = 14 test', () => {
        expect(operate("17","-3","+")).
            toBe("14"); 
    
    });
});

describe('Operate Multiplication Tests', ()=>{
    test('6 x 3 = 18 test', () => {
        expect(operate("6","3","x")).toBe("18"); 
    });
});
 
describe('Operate Division Tests', ()=>{
    test('-2/3 = -0.666 test', () => {
        expect(parseFloat(operate("-2","3","÷"))).
            toBeCloseTo(-0.666666667, 5); 
    });   
});


describe('Operate Subtraction Tests', ()=>{
    test('-6 -  3 = -9 test', () => {
        expect(operate("-6","3","-")).toBe("-9"); 
    });
    test('4 - 6 = -2 test', () => {
        expect(operate("4","6","-")).toBe("-2"); 
    });

    
});